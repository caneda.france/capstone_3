import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Contact from '../components/Contact';
import Footer from '../components/Footer';
import Comments from '../components/Comments';
import { useContext } from 'react';
import UserContext from '../UserContext';

export default function Home() {
	const guest = {
	    title: "Bi-maksu Takoyaki",
	    content: "Your favorite Japanese snack can now be delivered right at your doorstep.",
	    destination: "/products",
	    label: "Order Now!"}

	const userHome = {
	    title: "Welcome back, user!",
	    content: "Your favorite Japanese snack can now be delivered right at your doorstep. Place your order now!",
	    destination: "/products",
	    label: "Order Now!"}

	/*const [firstName, setFirstName] = useState([]);*/
	const {user} = useContext(UserContext);

	/*const retrieveFirstName = () => {

		 fetch("http://localhost:4000/users/details", {
		 	 method: "GET",
                headers: {
                    'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	firstName: firstName;
            })
         })
            .then(res => res.json())
            .then(data => {


          console.log(data)
      })
    }*/

   /* const fetchFirstName = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setFirstNAme(data.map(product => {})
		
		}, [])	*/
	return (
		
		(user.id !== null) 
		?		
		<Fragment>
			
			<Banner data={userHome} />
			
			<Highlights />
			<Comments />
			<h1 className="text-center"> Contact us</h1>
			<Contact/>
			{/*<button class="btn pmd-btn-fab pmd-ripple-effect btn-link" type="button">Link</button>*/}
			<Footer/>
		</Fragment>
		:
		<Fragment>
		
			<Banner data={guest} />
			<Highlights />
			<Comments />
			<h1 className="text-center"  style={{paddingTop: "2em"}}> Contact us</h1>
			<Contact/>
			{/*<button class="btn pmd-btn-fab pmd-ripple-effect btn-link" type="button">Link</button>*/}
			<Footer/>
		</Fragment>
)}