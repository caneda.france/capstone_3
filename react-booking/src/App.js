import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AdminDashboard from './pages/AdminDashboard';
import Products from './pages/Products'; 

import ProductsView from './pages/ProductsView';
import AdminHome from './pages/AdminHome';

import FAQ from './pages/Faq';


import { UserProvider } from './UserContext';

function App() {

   
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear();
    }

    
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])

    useEffect(() => {
      fetch("http://localhost:4000/users/details", {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
         
         if(data._id !== undefined) {

          
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });

         
         } else {
            setUser({
                id: null,
                isAdmin: null
            });
         }
          
      });
    }, []);

    return (
    
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <Container fluid>
            <AppNavbar/>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/adminDashboard" element={<AdminDashboard />} />
              <Route path="/products" element={<Products />} />
            
              <Route path="/products/:productId" element={<ProductsView />} />
              <Route path="/adminHome" element={<AdminHome />} />
              
              <Route path="/faq" element={<FAQ />} />
              
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    );
}

export default App;

